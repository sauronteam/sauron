 var contextPath = /*[[${#httpServletRequest.contextPath }]]*/ '';
    $( document ).ready(function(){
        $("#toggleButton").click(function(){
                if ($("#sidebar").is(':visible')){
                   $("#sidebar").hide("slow");
                   $("#page-wrapper").animate({
                        marginLeft: "0px"

                    },700);
                }
                else{
                   $("#sidebar").show("slow");
                   $("#page-wrapper").animate({
                        marginLeft: "250px"
                    },700);
                };
            });
        loadViews();
    });
    function loadViews(){
        $.ajax({
            url: contextPath+'/views',
            error: function() {
                displayNotification('glyphicon glyphicon-alert','There was an error retrieving the Views, contact the administrator','danger')
            },
            success: function(data) {
                DrawViews(data);
                displayNotification('glyphicon glyphicon-saved','Views loaded successfully','success')
            },
            type: 'get'
        });
    }

    function DrawViews(data){
        $.each(data,function(index){
            //console.log(data);
            if(index == 0){
                $("#tabList").append('<li role="presentation" class="active"><a href="#'+data[index].htmlId+'" aria-controls="rio" role="tab" data-toggle="tab">'+data[index].name+'</a></li>')
                $("#tabContent").append(' <div role="tabpanel" class="tab-pane active" id="'+data[index].htmlId+'"></div>');

                $.each(data[index].widgetList, function(widgetIndex){
                    //console.log(data[index].widgetList[widgetIndex]);
                    addAnyWidget(data[index].widgetList[widgetIndex].id, data[index].htmlId);
                });

            }else{
                $("#tabList").append('<li role="presentation"><a href="#'+data[index].htmlId+'" aria-controls="rio" role="tab" data-toggle="tab">'+data[index].name+'</a></li>')
                $("#tabContent").append(' <div role="tabpanel" class="tab-pane" id="'+data[index].htmlId+'"></div>');
                $.each(data[index].widgetList, function(widgetIndex){
                    //console.log(data[index].widgetList[widgetIndex]);
                    addAnyWidget(data[index].widgetList[widgetIndex].id, data[index].htmlId);
                });
            }
        });
    }

    function ApplicationDataDisplayModal(title, service, tableHeader){
        $("#genericDataDisplayTitle").html(title);
        $.ajax({
            url: contextPath+service,
            error: function() {
                displayNotification('glyphicon glyphicon-alert','There was an error retrieving the data, contact the administrator','danger')
            },
            success: function(data) {
                $("#genericDataDisplayBody").html(ApplicationDataTableRender(data, tableHeader));
            },
            type: 'get'
        });
        $('#genericDataDisplayModal').modal({
            backdrop: 'static',
            show: true
        });
    }

    function EndpointDataDisplayModal(title, service){
        $("#genericDataDisplayTitle").html(title);
        EndpointApplicationSelectorRender("#genericDataDisplayBody");

        $('#genericDataDisplayModal').modal({
            backdrop: 'static',
            show: true
        });
    }

    $("#viewAllEndpoints").click(function(){
        EndpointDataDisplayModal("Listing all endpoints added","/applications/1/endpoints");
    });

    $("#viewAllApplications").click(function(){
        var applicationsHeader = '<thead><tr><th>Application Name</th><th>Endpoint N°</th></tr></thead>';
        ApplicationDataDisplayModal("Listing all applications added","/applications", applicationsHeader);
    });

    function EndpointApplicationSelectorRender(modalBody){

        $.ajax({
            url: contextPath+'/applications',
            error: function() {
                displayNotification('glyphicon glyphicon-alert','There was an error retrieving the data, contact the administrator','danger')
            },
            success: function(data) {
                var applicationSelector;
                applicationSelector = '<select class="form-control" id="applicationSelector">';
                applicationSelector = applicationSelector + '<option value="0">Select an Application</option>';
                $.each(data,function(index){
                    applicationSelector = applicationSelector + '<option value="'+data[index].id+'">'+data[index].name+'</option>';
                });
                applicationSelector = applicationSelector + '</select>';
                applicationSelector = applicationSelector + '<div class="container><div class="row" id="endpointDataTable"></div></div>';
                //console.log(applicationSelector);
                $(modalBody).html(applicationSelector)
                $("#applicationSelector").change(function(){
                    if($("#applicationSelector option:selected").val() != "0"){
                        $.ajax({
                            url: contextPath+'/applications/'+$("#applicationSelector option:selected").val()+'/endpoints',
                            error: function() {
                                displayNotification('glyphicon glyphicon-alert','There was an error retrieving the data, contact the administrator','danger')
                            },
                            success: function(data) {
                                //console.log(data);
                                $("#genericDataDisplayBody").html(EndpointDataTableRender(data));
                            },
                            type: 'get'
                        });
                    }else{
                        $("#endpointDataTable").html('No endpoints were found for the selected application');
                    }
                })
            },
            type: 'get'
        });
    }

    function EndpointDataTableRender(data){
        var endpointHeader = '<thead><tr><th>Endpoint Name</th><th>Metric N°</th></tr></thead>';
        var table;
        table = '<table class="table table-striped table-bordered table-condensed viewAllTable">';
        table = table + endpointHeader;
        table = table + '<tbody>';
        $.each(data,function(index){
            table = table + '<tr>';
            table = table + '<td>'+data[index].url+'</td><td>'+data[index].metricList.length+'</td>';
            table = table + '</tr>';
        });
        table = table + '</tbody>';
        table = table + '</table>';
        //console.log(table);
        $("#endpointDataTable").html(table);
    }

    function ApplicationDataTableRender(data, header){
            var table;
            //console.log(data);
            table = '<table class="table table-striped table-bordered table-condensed">';
            table = table + header;
            table = table + '<tbody>';
            $.each(data,function(index){
                table = table + '<tr>';
                table = table + '<td>'+data[index].name+'</td><td>'+data[index].endpointList.length+'</td>';
                table = table + '</tr>';
            });
            return table;
        }

    $("#newApplicationSaveButton").click(function(){
        $.ajax({
           url: contextPath+'/applications',
           data: {
              applicationName: $("#applicationNameInput").val()
           },
           error: function() {
              displayNotification('glyphicon glyphicon-alert','There was an error adding the endpoint, contact the administrator','danger')
           },
           success: function(data) {
              $('#newApplicationModal').modal('hide');
              displayNotification('glyphicon glyphicon-saved','The application was added successfully','success')
           },
           type: 'post'
        });
    });

    $("#newViewSaveButton").click(function(){
        $.ajax({
           url: contextPath+'/views',
           data: {
              viewName: $("#viewNameInput").val()
           },
           error: function() {
              displayNotification('glyphicon glyphicon-alert','There was an error creating the view, contact the administrator','danger');
           },
           success: function(data) {
              $('#newViewModal').modal('hide');
              var viewName = $("#viewNameInput").val().replace(/\s/g,'_');
              $("#tabList").append('<li role="presentation"><a href="#'+viewName+'" aria-controls="'+viewName+'" role="tab" data-toggle="tab">'+$("#viewNameInput").val()+'</a></li>');
              $("#tabContent").append('<div role="tabpanel" class="tab-pane" id="'+viewName+'"></div>');
              displayNotification('glyphicon glyphicon-saved','The view was created successfully','success');
           },
           type: 'post'
        });
    });

    $("#newEndpointSaveButton").click(function(){
        $.ajax({
           url: contextPath+'/endpoints',
           data: {
              url: $("#endpointNameInput").val()
           },
           error: function() {
              displayNotification('glyphicon glyphicon-alert','There was an error adding the endpoint, contact the administrator','danger')
           },
           success: function(data) {
              $('#newEndointModal').modal('hide');
              var viewName = $("#viewNameInput").val().replace(/\s/g,'_');
              displayNotification('glyphicon glyphicon-saved','The endpoint was added successfully','success')
           },
           type: 'post'
        });
    });

    $("#widgetWizard").click(function(){
       addAnyWidget(1);
    });

    function addAnyWidget(id, viewId) {
        var color;
        var icon;
        var value;
        var name;

        $.ajax({
            url: contextPath + '/widgets/' + id,
            type: 'get',
            error: function () {
                displayNotification('glyphicon glyphicon-alert', 'There was an error retrieving the widget, contact the administrator', 'danger');
            },
            success: function (data) {
                //console.log(data);
                var actualMetricDate = data.metric.metricHistories[0].create;

                if (data.widgetType.id == 1) {
                    value = data.metric.metricHistories[0].value;
                    color = CalculateRangeColor(data.widgetProperties, value);
                    //console.log(color);
                    name = data.name;

                    if (color == "red") {
                        icon = "warning";
                    } else if (color == "green") {
                        icon = "check";
                    } else {
                        icon = "exclamation";
                    }
                    PlaceWidgetInView(viewId, DrawWidget(color, icon, value, name), "2");
                } else if (data.widgetType.id == 2) {
                    RenderGraph(viewId, name = data.name, id, data)
                }
            }

        });
    }

    function CalculateRangeColor(props, actualValue){
        var red_max, red_min;
        var green_max, green_min;
        var yellow_max, yellow_min;
        var selectedColor;
        $.each(props,function(propIndex){
            if(props[propIndex].name == "red_max"){
                red_max = props[propIndex].value;
            }else if(props[propIndex].name == "red_min"){
                red_min = props[propIndex].value;
            }else if(props[propIndex].name == "green_max"){
                green_max = props[propIndex].value;
            }else if(props[propIndex].name == "green_min"){
                green_min = props[propIndex].value;
            }else if(props[propIndex].name == "yellow_max"){
                yellow_max = props[propIndex].value;
            }else if(props[propIndex].name == "yellow_min"){
                yellow_min = props[propIndex].value;
            }
        });

        if(actualValue >= red_min){
            selectedColor = "red";
        }else if(actualValue <= green_max){
            selectedColor = "green";
        }else {
            selectedColor = "yellow";
        }
        return selectedColor;
    }

    function PlaceWidgetInView(view, widget, size){
        var viewId = "#"+view;
        var wrap;
        var insertedWidget = 0;
        var row = '<div class="row">__WIDGET__</div>';
        if(size == "2"){
            wrap = '<div class="col-lg-3 col-md-6">__WIDGET__</div>';
        }else{
             wrap = '<div class="col-lg-6 col-md-12">__WIDGET__</div>';
        }
        if($(viewId).find("div").length == 0){
            wrap = wrap.replace("__WIDGET__",widget);
            row = row.replace("__WIDGET__",wrap);
            $(viewId).append(row);
        }else{
            $(viewId+ " > div.row").each(function(){
                var twoWidgetTotal = $(this).find("div[class='col-lg-3 col-md-6']").length;
                var fourWidgetTotal = $(this).find("div[class='col-lg-6 col-md-12']").length;
                console.log(twoWidgetTotal + ";" + fourWidgetTotal);
                if(size == 2 && insertedWidget == 0){
                    if((twoWidgetTotal < 4 && fourWidgetTotal == 0) || ( twoWidgetTotal < 2 && fourWidgetTotal == 1)){
                        wrap = wrap.replace("__WIDGET__",widget);
                        $(this).append(wrap);
                        insertedWidget = 1;
                    }
                }else if(size == 4 && insertedWidget == 0){
                    if((twoWidgetTotal < 3 && fourWidgetTotal == 0) || (fourWidgetTotal == 1 && twoWidgetTotal == 0) ){
                        wrap = wrap.replace("__WIDGET__",widget);
                        $(this).append(wrap);
                        insertedWidget = 1;
                    }

                }
            });
            if(insertedWidget == 0){
               wrap = wrap.replace("__WIDGET__",widget);
               row = row.replace("__WIDGET__",wrap);
               $(viewId).append(row);
               insertedWidget = 1;
            }
        }
    }

    function getWidgetTemplate(){
            return '<div class="panel panel-__COLOR__">'
                   +'<div class="panel-heading">'
                   +'<div class="row">'
                   +'<div class="col-xs-3">'
                   +'<i class="fa fa-__ICON__ fa-5x"></i>'
                   +'</div>'
                   +'<div class="col-xs-9 text-right">'
                   +'<div class="huge">__VALUE__</div>'
                   +'<div>__NAME__</div>'
                   +'</div>'
                   +'</div>'
                   +'</div>'
                   +'<a href="#">'
                   +'<div class="panel-footer">'
                   +'<span class="pull-left">View Details</span>'
                   +'<span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>'
                   +'<div class="clearfix"></div>'
                   +'</div>'
                   +'</a>'
                   +'</div>';
        }

    function RenderGraph (view, title, id, data){
        var viewId = "#"+view;
        var insertedWidget = 0;
        var widget = PrepareGraphWidget(title,id);
        var row = '<div class="row">__WIDGET__</div>';

        if($(viewId).find("div").length == 0){
           row = row.replace("__WIDGET__",widget);
           $(viewId).append(row);
           insertedWidget = 1;
        }else{
            $(viewId+ " > div.row").each(function(){
                var twoWidgetTotal = $(this).find("div[class='col-lg-3 col-md-6']").length;
                var fourWidgetTotal = $(this).find("div[class='col-lg-6 col-md-12']").length;
                if(insertedWidget == 0){
                    if((twoWidgetTotal < 3 && fourWidgetTotal == 0) || (fourWidgetTotal == 1 && twoWidgetTotal == 0) ){
                        $(this).append(widget);
                        insertedWidget = 1;
                    }
                }
            });
        }
        if(insertedWidget == 0){
           row = row.replace("__WIDGET__",widget);
           $(viewId).append(row);
           insertedWidget = 1;
        }

        console.log(row);
        BuildGraphWidgetData(id, data);
    }

    function PrepareGraphWidget(title, id){
        var graphWidget = getGraphicWidgetTemplate();
        graphWidget = graphWidget.replace("__TITLE__",title);
        graphWidget = graphWidget.replace("__WIDGETID__",id);
        return graphWidget;
    }

    function BuildGraphWidgetData(htmlElement, data){
        var metrics = data.metric.metricHistories;
        console.log(metrics);
        new Morris.Bar({
          element: 'chart'+htmlElement,
          // Chart data records -- each entry in this array corresponds to a point on
          // the chart.
          data: metrics,
          // The name of the data record attribute that contains x-values.
          xkey: 'create',
          // A list of names of data record attributes that contain y-values.
          ykeys: ['value'],
          // Labels for the ykeys -- will be displayed when you hover over the
          // chart.
          labels: ['Value']
        });
    }

    function getGraphicWidgetTemplate(){
        return '<div class="col-lg-6 col-md-12">'
               +'<div class="panel panel-default">'
               +'<div class="panel-heading">'
               +'<i class="fa fa-bar-chart fa-fw"></i> __TITLE__'
               +'<div class="pull-right">'
               +'<div class="btn-group">'
               +'<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">'
               +'Actions'
               +'<span class="caret"></span>'
               +'</button>'
               +'<ul class="dropdown-menu pull-right" role="menu">'
               +'<li><a href="#">View Historic Data</a>'
               +'</li>'
               +'</ul>'
               +'</div>'
               +'</div>'
               +'</div>'
               +'<!-- /.panel-heading -->'
               +'<div class="panel-body">'
               +'<div id="chart__WIDGETID__" style=" height: 250px;"></div>'
               +'</div>'
               +'<!-- /.panel-body -->'
               +'</div></div></div>';
    }

    function DrawWidget(color,icon,value,name){
       /* var formattedWidget = $.get(contextPath+'/assets/templates/semaphore_widget.html',function(){
             var formattedWidget = widget.responseText;

            formattedWidget = formattedWidget.replace("__COLOR__", color);
            formattedWidget = formattedWidget.replace("__ICON__", icon);
            formattedWidget = formattedWidget.replace("__VALUE__", value);
            formattedWidget = formattedWidget.replace("__NAME__", name);
            return formattedWidget;
        });*/

        formattedWidget = getWidgetTemplate();
        formattedWidget = formattedWidget.replace("__COLOR__", color);
        formattedWidget = formattedWidget.replace("__ICON__", icon);
        formattedWidget = formattedWidget.replace("__VALUE__", value);
        formattedWidget = formattedWidget.replace("__NAME__", name);
       // console.log(formattedWidget);
        return formattedWidget;
    }

    //Draws facebook like notifications
    function displayNotification(icon,message, type){
        $.notify({
            //Options
            icon: icon,
            message: message
        },{
        //Settings
        type: type,
        placement: {
            from: "bottom",
            align: "right"
        },
        animate: {
            enter: 'animated fadeInUp',
            exit: 'animated fadeOutDown'
        },
        mouse_over: 'pause'
    });


}

    //Widget Wizard
    $("#btnStep1").on('click',function(){
        if ($('input[name=widgetType]:checked').val() ) {
            $('#step1Modal').modal('hide');
            $('#step1Moda2').modal('show');
        }else{
            $("#footerStep1").append('<p>You must select one kind of widget</p>');
        }
    });

    $("#btnStep2").on('click',function()
    {
        $('#step1Moda2').modal('hide');
        if($('input[name="widgetType"]:checked').val()=="1"){
            $('#step1Moda3A').modal('show');
        }else{
            $('#step1Moda3B').modal('show');
        }
    });

    $.ajax({
        url: contextPath+'/widgetstype'
    }).then(
        function(data) {
            $.each(data, function(i,value) {
                $("#radioDivSep1").append('<div class="radio"><label><input type="radio" name="widgetType" value="'+data[i].id+'"/>'+data[i].name+'</label></div>');
            });
    });

    $.ajax({
        url: "http://localhost:8080/widgetstype",
        data: '[{"id":1,"name":"Semaphore"},{"id":2,"name":"Graphic"}]'

    }).then(
        function(data) {
            var widget = $.parseJSON('[{"id":1,"name":"Metric 1"},{"id":2,"name":"Metric 2"}]')
            $.each(widget, function(i,value) {
                $("#metricsID").append('<option value="'+ widget[i].id+ '">'+widget[i].name+'</option>')
            });
        });

    $("#submitStep4B").on('click',function(){
        $.ajax({
            url: contextPath+'/widgets',
            type: 'POST',
            data: $( "form" ).serialize()
        });
      });

    $("#submitStep4A").on('click',function(){
        $.ajax({
            url: contextPath+'/widgets',
            type: 'POST',
            data: $( "form" ).serialize()
        });
    });

