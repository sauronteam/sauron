package com.equifax.hackaton.sauron.service.jmx.impl;

import com.equifax.hackaton.sauron.domain.Endpoint;
import com.equifax.hackaton.sauron.domain.Metric;
import com.equifax.hackaton.sauron.repository.MetricRepository;
import com.equifax.hackaton.sauron.service.jmx.JMXMetric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by jas16 on 11-06-2015.
 */
@Component
public class JMXMetricHandler
{

	@Autowired
	private MemoryMetric memoryMetric;

	@Autowired
	private OperatingSystemMetric operatingSystemMetric;

	@Autowired
	private ThreadingMetric threadingMetric;

	@Autowired
	MetricRepository metricRepository;

	private JMXConnector jmxConnector;

	public List<Metric> registerMetrics(Endpoint endpoint)
	{
		try
		{
			jmxConnector = JMXConnectorFactory.connect(new JMXServiceURL(endpoint.getUrl()), null);

			List<Metric> metrics =  resolveMetrics(endpoint);

			jmxConnector.close();

			return metrics;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		return Collections.emptyList();
	}

	private List<Metric> resolveMetrics(Endpoint endpoint)
	{
		List<Metric> resolvedMetrics = new ArrayList<>();

		for (JMXMetric metric : Arrays.asList(memoryMetric, operatingSystemMetric, threadingMetric))
		{
			resolvedMetrics.addAll(getAttributesAsMetrics(endpoint, metric));
		}

		return resolvedMetrics;
		//metricRepository.save(resolvedMetrics);
	}

	private List<Metric> getAttributesAsMetrics(Endpoint endpoint, JMXMetric metric)
	{
		List<Metric> attributesAsMetrics = new ArrayList<>();

		try
		{
			MBeanServerConnection mbeanConn = jmxConnector.getMBeanServerConnection();

			AttributeList attributes = mbeanConn
					.getAttributes(new ObjectName(metric.getMetricCategoryName()), metric.getMetricsNames());

			for (Attribute attribute : attributes.asList())
			{
				List<Metric> newMetrics = createMetric(endpoint, attribute,metric);
				attributesAsMetrics.addAll(newMetrics);
			}

			attributesAsMetrics.addAll(endpoint.getMetricList());
			endpoint.setMetricList(attributesAsMetrics);
		}
		catch (Exception e)
		{

		}

		return attributesAsMetrics;
	}


	public List<Metric> getParseResponse(AttributeList response,Endpoint endpoint)
	{
		List<Metric> attributesAsMetrics = new ArrayList<>();

		try
		{

			for (Attribute attribute : response.asList())
			{
				List<Metric> newMetrics = createMetric(endpoint, attribute);
				attributesAsMetrics.addAll(newMetrics);
			}

			attributesAsMetrics.addAll(endpoint.getMetricList());
			endpoint.setMetricList(attributesAsMetrics);
		}
		catch (Exception e)
		{

		}

		return attributesAsMetrics;
	}

	private List<Metric> createMetric(Endpoint endpoint, Attribute attribute,JMXMetric metric)
	{

		if (attribute.getValue() instanceof CompositeData)
		{
			List<Metric> list = new ArrayList<>();
			for (String submetricName : metric.getNestedMetricName())
			{
				if(((CompositeData) attribute.getValue()).get(submetricName) != null)
				{
					Metric newMetric = new Metric();
					newMetric.setEndpoint(endpoint);
					newMetric.setName(attribute.getName() +"."+submetricName);
					list.add(newMetric);
				}
			}
			return list;
		}
		else
		{
			Metric newMetric = new Metric();
			newMetric.setEndpoint(endpoint);
			newMetric.setName(attribute.getName());

			return Arrays.asList(newMetric);
		}

	}

	private List<Metric> createMetric(Endpoint endpoint, Attribute attribute)
	{
		for (JMXMetric metric : Arrays.asList(memoryMetric, operatingSystemMetric, threadingMetric))
		{

			if (Arrays.asList(metric.getMetricsNames()).contains(attribute.getName()))
			{
				if (attribute.getValue() instanceof CompositeData)
				{
					List<Metric> list = new ArrayList<>();
					for (String submetricName : metric.getNestedMetricName())
					{
						if (((CompositeData) attribute.getValue()).get(submetricName) != null)
						{
							Metric newMetric = new Metric();
							newMetric.setEndpoint(endpoint);
							newMetric.setName(attribute.getName() + "." + submetricName);
							list.add(newMetric);
						}
					}
					return list;
				}
				else
				{
					Metric newMetric = new Metric();
					newMetric.setEndpoint(endpoint);
					newMetric.setName(attribute.getName());

					return Arrays.asList(newMetric);
				}

			}
		}

		return Collections.EMPTY_LIST;
	}

}
