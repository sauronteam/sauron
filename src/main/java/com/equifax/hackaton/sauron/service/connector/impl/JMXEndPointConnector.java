package com.equifax.hackaton.sauron.service.connector.impl;

import com.equifax.hackaton.sauron.domain.Endpoint;
import com.equifax.hackaton.sauron.domain.Metric;
import com.equifax.hackaton.sauron.repository.MetricRepository;
import com.equifax.hackaton.sauron.service.connector.EndpointConnector;
import com.equifax.hackaton.sauron.service.jmx.JMXMetric;
import com.equifax.hackaton.sauron.service.jmx.impl.MemoryMetric;
import com.equifax.hackaton.sauron.service.jmx.impl.OperatingSystemMetric;
import com.equifax.hackaton.sauron.service.jmx.impl.ThreadingMetric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jas16 on 12-06-2015.
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class JMXEndPointConnector implements EndpointConnector<AttributeList>
{

	public MemoryMetric getMemoryMetric()
	{
		return memoryMetric;
	}

	public void setMemoryMetric(MemoryMetric memoryMetric)
	{
		this.memoryMetric = memoryMetric;
	}

	public OperatingSystemMetric getOperatingSystemMetric()
	{
		return operatingSystemMetric;
	}

	public void setOperatingSystemMetric(OperatingSystemMetric operatingSystemMetric)
	{
		this.operatingSystemMetric = operatingSystemMetric;
	}

	public ThreadingMetric getThreadingMetric()
	{
		return threadingMetric;
	}

	public void setThreadingMetric(ThreadingMetric threadingMetric)
	{
		this.threadingMetric = threadingMetric;
	}

	private MemoryMetric memoryMetric;

	private OperatingSystemMetric operatingSystemMetric;

	private ThreadingMetric threadingMetric;

	@Autowired
	MetricRepository metricRepository;

	private Endpoint endpoint;

	private JMXConnector jmxConnector;


	public JMXEndPointConnector(Endpoint endpoint)
	{
		this.endpoint = endpoint;
	}

	@Override public AttributeList getResponse()
	{

		AttributeList results = new AttributeList();
		try
		{
			jmxConnector = JMXConnectorFactory.connect(new JMXServiceURL(endpoint.getUrl()), null);

			MBeanServerConnection mbeanConn = jmxConnector.getMBeanServerConnection();

			for (JMXMetric metric : Arrays.asList(memoryMetric, operatingSystemMetric, threadingMetric))
			{
				results.addAll(mbeanConn
						.getAttributes(new ObjectName(metric.getMetricCategoryName()), metric.getMetricsNames()));
			}


			jmxConnector.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return results;
	}



	public void registerMetrics(Endpoint endpoint)
	{

		try
		{
			jmxConnector = JMXConnectorFactory.connect(new JMXServiceURL(endpoint.getUrl()), null);

			resolveMetrics(endpoint);

			jmxConnector.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private void resolveMetrics(Endpoint endpoint)
	{
		List<Metric> resolvedMetrics = new ArrayList<>();

		for (JMXMetric metric : Arrays.asList(memoryMetric, operatingSystemMetric, threadingMetric))
		{
			resolvedMetrics.addAll(getAttributesAsMetrics(endpoint, metric));
		}

		metricRepository.save(resolvedMetrics);
	}

	private List<Metric> getAttributesAsMetrics(Endpoint endpoint, JMXMetric metric)
	{
		List<Metric> attributesAsMetrics = new ArrayList<>();

		try
		{
			MBeanServerConnection mbeanConn = jmxConnector.getMBeanServerConnection();

			AttributeList attributes = mbeanConn
					.getAttributes(new ObjectName(metric.getMetricCategoryName()), metric.getMetricsNames());

			for (Attribute attribute : attributes.asList())
			{
				List<Metric> newMetrics = createMetric(endpoint, attribute,metric);
				attributesAsMetrics.addAll(newMetrics);
			}

			attributesAsMetrics.addAll(endpoint.getMetricList());
			endpoint.setMetricList(attributesAsMetrics);
		}
		catch (Exception e)
		{

		}

		return attributesAsMetrics;
	}

	private List<Metric> createMetric(Endpoint endpoint, Attribute attribute,JMXMetric metric)
	{

		if (attribute.getValue() instanceof CompositeData)
		{
			List<Metric> list = new ArrayList<>();
			for (String submetricName : metric.getNestedMetricName())
			{
				if(((CompositeData) attribute.getValue()).get(submetricName) != null)
				{
					Metric newMetric = new Metric();
					newMetric.setEndpoint(endpoint);
					newMetric.setName(attribute.getName() +"."+submetricName);
					list.add(newMetric);
				}
			}
			return list;
		}
		else
		{
			Metric newMetric = new Metric();
			newMetric.setEndpoint(endpoint);
			newMetric.setName(attribute.getName());

			return Arrays.asList(newMetric);
		}

	}
}
