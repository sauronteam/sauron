package com.equifax.hackaton.sauron.service.connector.impl;

import com.equifax.hackaton.sauron.domain.Endpoint;
import com.equifax.hackaton.sauron.service.connector.EndpointConnector;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Created by jas16 on 12-06-2015.
 */
@Component
@Scope (value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class RestEndpointConnector  implements EndpointConnector<ResponseEntity<String>>
{

	private Endpoint endpoint;

	public RestEndpointConnector(Endpoint endpoint)
	{
		this.endpoint = endpoint;
	}

	@Override public ResponseEntity<String> getResponse()
	{
		RestTemplate restTemplate = new RestTemplate();

		return restTemplate.getForEntity(endpoint.getUrl(), String.class);
	}
}
