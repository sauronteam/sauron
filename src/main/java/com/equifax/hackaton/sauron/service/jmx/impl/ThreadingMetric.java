package com.equifax.hackaton.sauron.service.jmx.impl;

import com.equifax.hackaton.sauron.service.jmx.JMXMetric;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by jas16 on 11-06-2015.
 */
@Component
@ConfigurationProperties (prefix = "metrics.threading", ignoreUnknownFields = false)
public class ThreadingMetric implements JMXMetric
{
	protected String metricCategoryName;
	protected String metricNames;

	public String[] getMetricsNames()
	{
		return metricNames.split(",");
	}

	public String getMetricCategoryName()
	{
		return metricCategoryName;
	}

	@Override public String[] getNestedMetricName()
	{
		return null;
	}

	public void setMetricCategoryName(String metricCategoryName)
	{
		this.metricCategoryName = metricCategoryName;
	}

	public String getMetricNames()
	{
		return metricNames;
	}

	public void setMetricNames(String metricNames)
	{
		this.metricNames = metricNames;
	}
}
