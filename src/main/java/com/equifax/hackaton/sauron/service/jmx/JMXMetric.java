package com.equifax.hackaton.sauron.service.jmx;

/**
 * Created by jas16 on 11-06-2015.
 */
public interface JMXMetric
{
	 String[] getMetricsNames();
	 String getMetricCategoryName();
	String[] getNestedMetricName();

}
