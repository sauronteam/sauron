package com.equifax.hackaton.sauron.service.connector;

import com.equifax.hackaton.sauron.domain.Endpoint;
import com.equifax.hackaton.sauron.service.connector.impl.JMXEndPointConnector;
import com.equifax.hackaton.sauron.service.connector.impl.RestEndpointConnector;
import com.equifax.hackaton.sauron.service.jmx.impl.MemoryMetric;
import com.equifax.hackaton.sauron.service.jmx.impl.OperatingSystemMetric;
import com.equifax.hackaton.sauron.service.jmx.impl.ThreadingMetric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by jas16 on 12-06-2015.
 */
@Component
public class EndpointConnectorFactory
{


	@Autowired
	private MemoryMetric memoryMetric;

	@Autowired
	private OperatingSystemMetric operatingSystemMetric;

	@Autowired
	private ThreadingMetric threadingMetric;

	private static EndpointConnectorFactory instance = new EndpointConnectorFactory();

	private EndpointConnectorFactory(){}



	public static EndpointConnectorFactory getInstance(){  return instance; }

	public EndpointConnector createConnector( Endpoint endpoint)
	{
		if(endpoint.getUrl().contains("jmx"))
		{
			JMXEndPointConnector connector = new JMXEndPointConnector(endpoint);
			connector.setMemoryMetric(memoryMetric);
			connector.setThreadingMetric(threadingMetric);
			connector.setOperatingSystemMetric(operatingSystemMetric);

			return connector;
		}

		return  new RestEndpointConnector(endpoint );
	}

}
