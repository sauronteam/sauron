package com.equifax.hackaton.sauron.service.impl;

import com.equifax.hackaton.sauron.domain.Endpoint;
import com.equifax.hackaton.sauron.domain.Metric;
import com.equifax.hackaton.sauron.domain.MetricHistory;
import com.equifax.hackaton.sauron.repository.EndpointRepository;
import com.equifax.hackaton.sauron.repository.MetricHistoryRepository;
import com.equifax.hackaton.sauron.repository.MetricRepository;
import com.equifax.hackaton.sauron.service.EndpointService;
import com.equifax.hackaton.sauron.service.MetricService;
import com.equifax.hackaton.sauron.service.connector.EndpointConnector;
import com.equifax.hackaton.sauron.service.connector.EndpointConnectorFactory;
import com.equifax.hackaton.sauron.service.connector.impl.RestEndpointConnector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonSimpleJsonParser;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import javax.management.AttributeList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */


@Component
public class EndpointServiceImpl implements EndpointService
{

	@Autowired
	private EndpointRepository endpointRepository;

	@Autowired
	private MetricRepository metricRepository;

	@Autowired
	private MetricHistoryRepository metricHistoryRepository;

	@Autowired
	private MetricServiceImpl metricService;

	@Autowired
	private EndpointConnectorFactory endpointConnectorFactory;

	@Override public void save(Endpoint endpoint)
	{
		endpointRepository.save(endpoint);
	}

	@Override public Endpoint discoverEndpoint(Endpoint undiscoverEndpoint)
	{

		EndpointConnector connector = endpointConnectorFactory.createConnector(undiscoverEndpoint);
		List<Metric> parsedMetrics = Collections.EMPTY_LIST;


		parsedMetrics = metricService.parseMetrics(connector.getResponse(),undiscoverEndpoint);


		bindEndpoint(undiscoverEndpoint, parsedMetrics);

		undiscoverEndpoint.setMetricList(parsedMetrics);

		metricRepository.save(parsedMetrics);
		endpointRepository.save(undiscoverEndpoint);

		return undiscoverEndpoint;
	}

	private void bindEndpoint(Endpoint undiscoverEndpoint, List<Metric> parsedMetrics)
	{
		for(Metric metric : parsedMetrics)
		{
			metric.setEndpoint(undiscoverEndpoint);
		}
	}

	public Integer captureMetricsSnaphshot(Endpoint endpoint)
	{
		EndpointConnector connector  = endpointConnectorFactory.createConnector(endpoint);

		Object response=  connector.getResponse();
		List<MetricHistory> metrics = new ArrayList<MetricHistory>();

		if(response instanceof AttributeList)
		{
			metrics =	metricService.parseMetricsSnapshot((AttributeList) response, endpoint);

		}else
		{
			metrics = metricService.parseMetricsSnapshot((ResponseEntity<String>) response, endpoint);
		}

		metricHistoryRepository.save(metrics);

		return metrics.size();


	}
}
