package com.equifax.hackaton.sauron.service;

import com.equifax.hackaton.sauron.domain.Endpoint;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
public interface EndpointService
{

	void save(Endpoint endpoint);

	public Endpoint discoverEndpoint(Endpoint undiscoverEndpoint);

}
