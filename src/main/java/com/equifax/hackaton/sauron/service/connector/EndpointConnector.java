package com.equifax.hackaton.sauron.service.connector;

/**
 * Created by jas16 on 12-06-2015.
 */
public interface EndpointConnector<T>
{
	T getResponse();
}
