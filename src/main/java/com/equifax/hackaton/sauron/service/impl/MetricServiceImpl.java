package com.equifax.hackaton.sauron.service.impl;

import com.equifax.hackaton.sauron.domain.Endpoint;
import com.equifax.hackaton.sauron.domain.Metric;
import com.equifax.hackaton.sauron.domain.MetricHistory;
import com.equifax.hackaton.sauron.service.jmx.JMXMetric;
import com.equifax.hackaton.sauron.service.jmx.impl.JMXMetricHandler;
import com.equifax.hackaton.sauron.service.jmx.impl.MemoryMetric;
import com.equifax.hackaton.sauron.service.jmx.impl.OperatingSystemMetric;
import com.equifax.hackaton.sauron.service.jmx.impl.ThreadingMetric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonSimpleJsonParser;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.openmbean.CompositeData;
import java.util.*;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */

@Component
public class MetricServiceImpl
{

	@Autowired
	private MemoryMetric memoryMetric;

	@Autowired
	private OperatingSystemMetric operatingSystemMetric;

	@Autowired
	private ThreadingMetric threadingMetric;

	@Autowired
	private JMXMetricHandler jmxMetricHandler;

	public List<Metric> parseMetrics(Object response,Endpoint endpoint)
	{
		if(response instanceof ResponseEntity)
		{

			String jsonResponse = ((ResponseEntity<String>)response).getBody();
			JsonSimpleJsonParser parser = new JsonSimpleJsonParser();
			Map<String, Object> parsedResponse = parser.parseMap(jsonResponse);

			List<Metric> list = new ArrayList<Metric>();

			for (String metricName : parsedResponse.keySet())
			{
				Metric newMetric = new Metric();
				newMetric.setName(metricName);
				list.add(newMetric);
			}

			return list;

		}
		else
		{
		return jmxMetricHandler.getParseResponse((AttributeList) response,endpoint);

		}

	}



	public List<MetricHistory> parseMetricsSnapshot(ResponseEntity<String> response,Endpoint endpoint)
	{
		String jsonResponse = response.getBody();
		JsonSimpleJsonParser parser = new JsonSimpleJsonParser();
		Map<String,Object> parsedResponse  = parser.parseMap(jsonResponse);

		List<MetricHistory> list = new ArrayList<MetricHistory>();

		for(Map.Entry<String,Object> metric : parsedResponse.entrySet())
		{
			if(getMetricFromEndpoint(endpoint,metric.getKey())!=null)
			{
				MetricHistory newMetric = new MetricHistory();
				newMetric.setValue((String) metric.getValue());
				newMetric.setMetric(getMetricFromEndpoint(endpoint, metric.getKey()));
				list.add(newMetric);
			}

		}

		return list;
	}

	private Metric getMetricFromEndpoint(Endpoint endpoint, String metricName)
	{
		for(Metric metric : endpoint.getMetricList())
		{
			if(metric.getName().equalsIgnoreCase(metricName))
				return metric;
		}

		return null;

	}

	public List<MetricHistory> parseMetricsSnapshot(AttributeList response,Endpoint endpoint)
	{
		List<MetricHistory> list = new ArrayList<MetricHistory>();


		for(Attribute metric : response.asList())
		{

			list.addAll(createMetric(endpoint,metric,getJMXMetric(metric.getName())));

		}

		return list;

	}

	private JMXMetric getJMXMetric(String name)
	{
		for(JMXMetric jmxMetric : Arrays.asList(threadingMetric,memoryMetric,operatingSystemMetric))
		{
			if(Arrays.asList(jmxMetric.getMetricsNames()).contains(name))
				return jmxMetric;

		}

		return null;
	}

	private List<MetricHistory> createMetric(Endpoint endpoint, Attribute attribute,JMXMetric metric)
	{

		if (attribute.getValue() instanceof CompositeData)
		{
			List<MetricHistory> list = new ArrayList<MetricHistory>();
			for (String submetricName : metric.getNestedMetricName())
			{
				if(((CompositeData) attribute.getValue()).get(submetricName) != null)
				{
					MetricHistory newMetric = new MetricHistory();
					newMetric.setMetric(getMetricFromEndpoint(endpoint,attribute.getName() + "." + submetricName));
					if(((CompositeData) attribute.getValue()).get(submetricName) instanceof  Number)
					{
						newMetric.setValue(String.valueOf (((CompositeData) attribute.getValue()).get(submetricName)));
					}
					else
					{
						if(((CompositeData) attribute.getValue()).get(submetricName) instanceof  String)
						{
							newMetric.setValue(String.valueOf (((CompositeData) attribute.getValue()).get(submetricName)));
						}
					}
					list.add(newMetric);
				}
			}
			return list;
		}
		else
		{
			MetricHistory newMetric = new MetricHistory();
			newMetric.setMetric(getMetricFromEndpoint(endpoint, attribute.getName()));
			newMetric.setValue((String.valueOf(attribute.getValue())));

			return Arrays.asList(newMetric);
		}

	}


}
