package com.equifax.hackaton.sauron.sampler;

import com.equifax.hackaton.sauron.domain.Endpoint;
import com.equifax.hackaton.sauron.domain.Metric;
import com.equifax.hackaton.sauron.repository.EndpointRepository;
import com.equifax.hackaton.sauron.service.impl.EndpointServiceImpl;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * Created by jas16 on 12-06-2015.
 */
@Component
public class MetricSampler
{

	@Autowired
	private EndpointServiceImpl service;


	@Autowired
	private EndpointRepository endpointRepository;

	@Scheduled (fixedRate = 15000)
	public void sampleMetrics()
	{
		Iterable<Endpoint> endpoints = endpointRepository.findAll();

		  Iterable<Integer> updatedEndpoints =    Observable.from(newArrayList(endpoints))
				.flatMap(endpoint -> checkMetricList(endpoint).subscribeOn(Schedulers.io()))
						.toBlocking().toIterable();

		//TODO:call any service to user the updatedMetrics
		updatedEndpoints.forEach(x ->System.out.println(x));
	}


	private Observable<Integer> checkMetricList(Endpoint endpoint)
	{
		return Observable.create(new Observable.OnSubscribe<Integer>() {
			public void call(Subscriber<? super Integer> subscriber) {
				if(!subscriber.isUnsubscribed())
				{
					subscriber.onNext(service.captureMetricsSnaphshot(endpoint));
					subscriber.onCompleted();
				}

			}
		});
	}
}
