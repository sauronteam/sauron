package com.equifax.hackaton.sauron.util;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */

public class Encoder implements PasswordEncoder
{

	@Override
	public String encode(CharSequence charSequence)
	{
		return charSequence.toString();
	}

	@Override
	public boolean matches(CharSequence charSequence, String s)
	{
		return encode(charSequence).equals(s);
	}
}
