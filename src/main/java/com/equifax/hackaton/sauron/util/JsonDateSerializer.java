package com.equifax.hackaton.sauron.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Component
public class JsonDateSerializer extends JsonSerializer<Date>
{

	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm");

	@Override

	public void serialize(Date date, JsonGenerator gen, SerializerProvider provider)

			throws IOException, JsonProcessingException
	{

		String formattedDate = dateFormat.format(date);

		gen.writeString(formattedDate);

	}

}
