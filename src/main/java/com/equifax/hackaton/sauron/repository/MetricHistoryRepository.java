package com.equifax.hackaton.sauron.repository;

import com.equifax.hackaton.sauron.domain.MetricHistory;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
public interface MetricHistoryRepository extends CrudRepository<MetricHistory, Long>
{

	MetricHistory findFirstByMetricIdOrderByCreateDesc(Long metricId);

	List<MetricHistory> findByMetricIdOrderByCreateDesc(Long metricId);
}
