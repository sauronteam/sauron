package com.equifax.hackaton.sauron.repository;

import com.equifax.hackaton.sauron.domain.WidgetProperty;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
public interface WidgetPropertyRepository extends CrudRepository<WidgetProperty, Long>
{

}
