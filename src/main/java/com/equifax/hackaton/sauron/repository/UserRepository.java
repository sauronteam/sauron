package com.equifax.hackaton.sauron.repository;

import com.equifax.hackaton.sauron.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */

public interface UserRepository extends CrudRepository<User, Long>
{

	User findByUsername(String username);
}
