package com.equifax.hackaton.sauron.repository;

import com.equifax.hackaton.sauron.domain.Metric;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
public interface MetricRepository extends CrudRepository<Metric, Long>
{

	List<Metric> findByEndpointId(Long endpointId);
}
