package com.equifax.hackaton.sauron.repository;

import com.equifax.hackaton.sauron.domain.View;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */

public interface ViewRepository extends CrudRepository<View, Long>
{

}
