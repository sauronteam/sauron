package com.equifax.hackaton.sauron.repository;

import com.equifax.hackaton.sauron.domain.Widget;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */

public interface WidgetRepository extends CrudRepository<Widget, Long>
{

	List<Widget> findByMetricId(Long metricId);
}
