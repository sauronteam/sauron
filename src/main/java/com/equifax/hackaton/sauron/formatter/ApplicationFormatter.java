package com.equifax.hackaton.sauron.formatter;

import com.equifax.hackaton.sauron.domain.Application;
import com.equifax.hackaton.sauron.repository.ApplicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.Locale;

/**
 * Created by jas16 on 10-06-2015.
 */
@Component
public class ApplicationFormatter implements Formatter<Application>
{
	@Autowired
	private ApplicationRepository applicationRepository;

	@Override public Application parse(String s, Locale locale) throws ParseException
	{
		return applicationRepository.findOne(Long.valueOf(s));
	}

	@Override public String print(Application application, Locale locale)
	{
		return String.valueOf(application.getId());
	}
}
