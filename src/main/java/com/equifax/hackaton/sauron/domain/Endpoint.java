package com.equifax.hackaton.sauron.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Entity
@Table (name = "ENDPOINT")
public class Endpoint implements Serializable
{

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "endpoint_id")
	private Long id;

	@JsonIgnore
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "application", nullable = false)
	private Application application;

	@OneToMany (fetch = FetchType.EAGER, mappedBy = "endpoint", cascade = CascadeType.ALL)
	private List<Metric> metricList;

	private String url;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Application getApplication()
	{
		return application;
	}

	public void setApplication(Application application)
	{
		this.application = application;
	}

	public List<Metric> getMetricList()
	{
		return metricList;
	}

	public void setMetricList(List<Metric> metricList)
	{
		this.metricList = metricList;
	}

	public String getUrl()
	{
		return url;
	}

	public void setUrl(String url)
	{
		this.url = url;
	}
}
