package com.equifax.hackaton.sauron.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Entity
@Table(name = "FREQUENCYREFRESH")
public class FrequencyRefresh implements Serializable
{

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "widget_id")
	private Long id;

	private String name;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
}
