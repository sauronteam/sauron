package com.equifax.hackaton.sauron.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Entity
@Table(name = "USER")
public class User implements Serializable
{

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "user_id")
	private Long id;

	private String username;

	private String password;

	private String rol;

	@OneToMany (fetch = FetchType.LAZY, mappedBy = "user", cascade = CascadeType.ALL)
	private List<Application> applicationList;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public String getRol()
	{
		return rol;
	}

	public void setRol(String rol)
	{
		this.rol = rol;
	}

	public List<Application> getApplicationList()
	{
		return applicationList;
	}

	public void setApplicationList(List<Application> applicationList)
	{
		this.applicationList = applicationList;
	}
}
