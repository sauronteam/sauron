package com.equifax.hackaton.sauron.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Entity
@Table (name = "VIEW")
public class View implements Serializable
{

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "view_id")
	private Long id;

	private String name;
	@Column(name = "HTMLID")
	private String htmlId;
	@Column(name = "GRIDSTACKDISTRIBUTION")
	private String gridStackDistribution;

	@OneToMany (fetch = FetchType.LAZY, mappedBy = "view", cascade = CascadeType.ALL)
	private List<Widget> widgetList;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getHtmlId()
	{
		return htmlId;
	}

	public void setHtmlId(String htmlId)
	{
		this.htmlId = htmlId;
	}

	public List<Widget> getWidgetList()
	{
		return widgetList;
	}

	public void setWidgetList(List<Widget> widgetList)
	{
		this.widgetList = widgetList;
	}

	public String getGridStackDistribution()
	{
		return gridStackDistribution;
	}

	public void setGridStackDistribution(String gridStackDistribution)
	{
		this.gridStackDistribution = gridStackDistribution;
	}
}
