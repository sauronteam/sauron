package com.equifax.hackaton.sauron.domain;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */

public class UserDetail extends org.springframework.security.core.userdetails.User
{

	private Long userId;

	public UserDetail(String username, String password, Collection<? extends GrantedAuthority> authorities)
	{
		super(username, password, authorities);
	}

	public Long getUserId()
	{
		return userId;
	}

	public void setUserId(Long userId)
	{
		this.userId = userId;
	}
}
