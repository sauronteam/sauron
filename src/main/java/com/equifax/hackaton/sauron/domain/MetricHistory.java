package com.equifax.hackaton.sauron.domain;

import com.equifax.hackaton.sauron.util.JsonDateSerializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Entity
@Table (name = "METRICHISTORY")
public class MetricHistory implements Serializable
{

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "metric_history_id")
	private Long id;

	@JsonIgnore
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "metric", nullable = false)
	private Metric metric;

	private String value;

	@JsonSerialize (using = JsonDateSerializer.class)
	private Date create;

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	@PrePersist
	public void prePersist()
	{
		create = new Date();
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Metric getMetric()
	{
		return metric;
	}

	public void setMetric(Metric metric)
	{
		this.metric = metric;
	}

	public Date getCreate()
	{
		return create;
	}

	public void setCreate(Date create)
	{
		this.create = create;
	}
}
