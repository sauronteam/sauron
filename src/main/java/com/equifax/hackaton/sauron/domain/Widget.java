package com.equifax.hackaton.sauron.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Entity
@Table (name = "WIDGET")
public class Widget implements Serializable
{

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "widget_id")
	private Long id;

	private String name;

	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "WIDGETTYPE", nullable = false)
	private WidgetType widgetType;

	@JsonIgnore
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "view", nullable = false)
	private View view;

	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "metric", nullable = false)
	private Metric metric;

	private String description;

	@Column (name = "HTMLID")
	private String htmlId;

	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "FREQUENCYREFRESH", nullable = false)
	private FrequencyRefresh frequencyRefresh;

	@OneToMany (fetch = FetchType.LAZY, mappedBy = "widget", cascade = CascadeType.ALL)
	private List<WidgetProperty> widgetProperties;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public WidgetType getWidgetType()
	{
		return widgetType;
	}

	public void setWidgetType(WidgetType widgetType)
	{
		this.widgetType = widgetType;
	}

	public Metric getMetric()
	{
		return metric;
	}

	public View getView()
	{
		return view;
	}

	public void setView(View view)
	{
		this.view = view;
	}

	public void setMetric(Metric metric)
	{
		this.metric = metric;
	}

	public FrequencyRefresh getFrequencyRefresh()
	{
		return frequencyRefresh;
	}

	public void setFrequencyRefresh(FrequencyRefresh frequencyRefresh)
	{
		this.frequencyRefresh = frequencyRefresh;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public List<WidgetProperty> getWidgetProperties()
	{
		return widgetProperties;
	}

	public void setWidgetProperties(List<WidgetProperty> widgetProperties)
	{
		this.widgetProperties = widgetProperties;
	}

	public String getHtmlId()
	{
		return htmlId;
	}

	public void setHtmlId(String htmlId)
	{
		this.htmlId = htmlId;
	}
}
