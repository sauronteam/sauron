package com.equifax.hackaton.sauron.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by jas16 on 10-06-2015.
 */

@Entity
@Table (name = "WIDGETPROPERTY")
public class WidgetProperty implements Serializable
{

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "widget_property_id")
	private Long id;

	@JsonIgnore
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "WIDGET", nullable = false)
	private Widget widget;

	private String name;

	private String value;

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getValue()
	{
		return value;
	}

	public void setValue(String value)
	{
		this.value = value;
	}

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public Widget getWidget()
	{
		return widget;
	}

	public void setWidget(Widget widget)
	{
		this.widget = widget;
	}
}
