package com.equifax.hackaton.sauron.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Entity
@Table (name = "APPLICATION")
public class Application implements Serializable
{

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "application_id")
	private Long id;

	@JsonIgnore
	@ManyToOne (fetch = FetchType.LAZY)
	@JoinColumn (name = "user", nullable = false)
	private User user;

	private String name;


	@OneToMany (fetch = FetchType.LAZY, mappedBy = "application", cascade = CascadeType.ALL)
	private List<Endpoint> endpointList;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public User getUser()
	{
		return user;
	}

	public void setUser(User user)
	{
		this.user = user;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<Endpoint> getEndpointList()
	{
		return endpointList;
	}

	public void setEndpointList(List<Endpoint> endpointList)
	{
		this.endpointList = endpointList;
	}
}
