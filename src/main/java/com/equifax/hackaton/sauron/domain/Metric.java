package com.equifax.hackaton.sauron.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Entity
@Table (name = "METRIC")
public class Metric implements Serializable
{

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	@Column (name = "metric_id")
	private Long id;

	private String name;

	@Column (name = "CUSTOMNAME")
	private String customName;

	@JsonIgnore
	@ManyToOne (fetch = FetchType.EAGER)
	@JoinColumn (name = "endpoint", nullable = false)
	private Endpoint endpoint;

	@JsonIgnore
	@OneToMany (fetch = FetchType.LAZY, mappedBy = "metric", cascade = CascadeType.ALL)
	private List<Widget> widgetList;

	@OneToMany (fetch = FetchType.LAZY, mappedBy = "metric", cascade = CascadeType.ALL)
	@OrderBy ("create DESC")
	private List<MetricHistory> metricHistories;

	public Long getId()
	{
		return id;
	}

	public void setId(Long id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Endpoint getEndpoint()
	{
		return endpoint;
	}

	public void setEndpoint(Endpoint endpoint)
	{
		this.endpoint = endpoint;
	}

	public List<Widget> getWidgetList()
	{
		return widgetList;
	}

	public void setWidgetList(List<Widget> widgetList)
	{
		this.widgetList = widgetList;
	}

	public String getCustomName()
	{
		return customName;
	}

	public void setCustomName(String customName)
	{
		this.customName = customName;
	}

	public List<MetricHistory> getMetricHistories()
	{
		return metricHistories;
	}

	public void setMetricHistories(List<MetricHistory> metricHistories)
	{
		this.metricHistories = metricHistories;
	}
}
