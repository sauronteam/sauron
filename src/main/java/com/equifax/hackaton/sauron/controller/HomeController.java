package com.equifax.hackaton.sauron.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Controller
@Scope ("session")
@RequestMapping (HomeController.ENDPOIT)
public class HomeController
{

	public static final String ENDPOIT = "/";

	@RequestMapping (method = RequestMethod.GET)
	public String index()
	{
		return "index";
	}

	@RequestMapping (value = "/login", method = RequestMethod.GET)
	public String login()
	{
		return "login";
	}

	@RequestMapping (value = "/logout", method = RequestMethod.GET)
	public String logout()
	{
		return "login";
	}
}
