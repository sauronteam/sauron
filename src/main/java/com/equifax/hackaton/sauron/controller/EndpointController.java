package com.equifax.hackaton.sauron.controller;

import com.equifax.hackaton.sauron.domain.Application;
import com.equifax.hackaton.sauron.domain.Endpoint;
import com.equifax.hackaton.sauron.repository.ApplicationRepository;
import com.equifax.hackaton.sauron.repository.EndpointRepository;
import com.equifax.hackaton.sauron.service.EndpointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Controller
@Scope ("session")
@RequestMapping (ApplicationController.ENDPOINT)
public class EndpointController
{

	public static final String ENDPOINT = "/endpoints";

	@Autowired
	private ApplicationRepository applicationRepository;

	@Autowired
	private EndpointRepository endpointsRepository;

	@Autowired
	private EndpointService endpointService;

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "/{applicationId}" + ENDPOINT, method = RequestMethod.GET)
	public
	@ResponseBody
	Iterable<Endpoint> findAll(@PathVariable Long applicationId)
	{
		return endpointsRepository.findByApplicationId(applicationId);
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "/{applicationId}" + ENDPOINT + "/{endpointId}", method = RequestMethod.GET)
	public
	@ResponseBody
	Endpoint findOne(@PathVariable Long endpointId)
	{
		return endpointsRepository.findOne(endpointId);
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "/{applicationId}/" + ENDPOINT + "/new", method = RequestMethod.GET)
	public String newEndpoint(@PathVariable Long applicationId, Model model)
	{
		Application application = applicationRepository.findOne(applicationId);
		Endpoint newEndpoint = new Endpoint();

		newEndpoint.setApplication(application);

		model.addAttribute("endpoint", newEndpoint);
		model.addAttribute("aplicationId", application.getId());

		return "endpointForm";
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "/{applicationId}/" + ENDPOINT, method = RequestMethod.POST)
	public
	@ResponseBody
	Integer saveEndpoint(@PathVariable Long applicactionId, @RequestParam ("url") String url)
	{
		Endpoint endpoint = new Endpoint();
		endpoint.setUrl(url);
		endpoint.setApplication(applicationRepository.findOne(applicactionId));
		return endpointService.discoverEndpoint(endpoint).getMetricList().size();
	}

}




