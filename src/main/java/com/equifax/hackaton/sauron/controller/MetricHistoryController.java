package com.equifax.hackaton.sauron.controller;

import com.equifax.hackaton.sauron.domain.MetricHistory;
import com.equifax.hackaton.sauron.repository.MetricHistoryRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@RestController
@RequestMapping (ApplicationController.ENDPOINT)
public class MetricHistoryController
{

	public static final String ENDPOINT = "/metrichistories";

	@Autowired
	private MetricHistoryRepository metricHistoryRepository;

	@RequestMapping (value = "{applicationId}/" + EndpointController.ENDPOINT + "/{endpointId}/" + MetricController.ENDPOINT
			+ "/{metricId}/" + ENDPOINT + "/{metricHistoryId}", method = RequestMethod.GET)
	public MetricHistory findOne(@PathVariable Long metricHistoryId)
	{
		return metricHistoryRepository.findOne(metricHistoryId);
	}

	@RequestMapping (value = "{applicationId}/" + EndpointController.ENDPOINT + "/{endpointId}/" + MetricController.ENDPOINT
			+ "/{metricId}/" + ENDPOINT, method = RequestMethod.GET)
	public Iterable<MetricHistory> findByMetric(@PathVariable Long metricId,
			@RequestParam (value = "action", required = false) String action)
	{
		if (action!=null&&action.equals("last"))
		{
			return Lists.newArrayList(metricHistoryRepository.findFirstByMetricIdOrderByCreateDesc(metricId));
		}
		else
		{
			return metricHistoryRepository.findByMetricIdOrderByCreateDesc(metricId);
		}
	}

}
