package com.equifax.hackaton.sauron.controller;

import com.equifax.hackaton.sauron.domain.Metric;
import com.equifax.hackaton.sauron.repository.MetricRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@RestController
@RequestMapping (ApplicationController.ENDPOINT)
public class MetricController
{

	public static final String ENDPOINT = "/metrics";

	@Autowired
	private MetricRepository metricRepository;

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "/{applicationId}" + EndpointController.ENDPOINT + "/{endpointId}"
			+ ENDPOINT, method = RequestMethod.GET)
	public
	@ResponseBody
	Iterable<Metric> findAll(@PathVariable Long applicationId, @PathVariable Long endpointId)
	{
		return metricRepository.findByEndpointId(endpointId);
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "/{applicationId}/" + EndpointController.ENDPOINT + "/{endpointId}/" + ENDPOINT
			+ "/{metricId}", method = RequestMethod.GET)
	public
	@ResponseBody
	Metric findOne(@PathVariable Long applicationId, @PathVariable Long endpointId, @PathVariable Long metricId)
	{
		return metricRepository.findOne(metricId);
	}

}
