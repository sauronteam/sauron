package com.equifax.hackaton.sauron.controller;

import com.equifax.hackaton.sauron.domain.Widget;
import com.equifax.hackaton.sauron.domain.WidgetProperty;
import com.equifax.hackaton.sauron.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.UUID;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Controller
@RequestMapping (WidgetController.ENDPOINT)
public class WidgetController
{

	public static final String ENDPOINT = "/widgets";

	@Autowired
	private WidgetTypeRepository widgetTypeRepository;

	@Autowired
	private FrequencyRefreshRepository frequencyRefreshRepository;

	@Autowired
	private MetricRepository metricRepository;

	@Autowired
	private WidgetPropertyRepository widgetPropertyRepository;

	@Autowired
	private WidgetRepository widgetRepository;

	@Autowired
	private ViewRepository viewRepository;

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "", method = RequestMethod.POST)
	public
	@ResponseBody
	Widget saveEndpoint(@RequestParam (value = "widgetType", required = true) Long widgetType,
			@RequestParam (value = "metricsID", required = true) Long metricsId,
			@RequestParam (value = "viewId", required = true) Long viewId,
			@RequestParam (value = "frequencyRefresh", required = true) Long frequencyRefresh,
			@RequestParam (value = "description", required = false) String description,
			@RequestParam (value = "widgetName", required = true) String widgetName,
			@RequestParam (value = "propertiesList", required = true) String[] protertiesList)
	{
		Widget widget = new Widget();
		widget.setWidgetType(widgetTypeRepository.findOne(widgetType));
		widget.setMetric(metricRepository.findOne(metricsId));
		widget.setFrequencyRefresh(frequencyRefreshRepository.findOne(frequencyRefresh));
		widget.setView(viewRepository.findOne(viewId));
		widget.setName(widgetName);
		widget.setDescription(description);
		widget.setHtmlId(widgetName + "_" + UUID.randomUUID().toString());
		widget.setWidgetProperties(new ArrayList());
		widgetRepository.save(widget);
		for (String property : protertiesList)
		{
			String[] prop = property.split("|");
			WidgetProperty widgetProperty = new WidgetProperty();
			widgetProperty.setWidget(widget);
			widgetProperty.setName(prop[0]);
			widgetProperty.setValue(prop[1]);
			widgetPropertyRepository.save(widgetProperty);
		}
		return widget;
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "", method = RequestMethod.GET)
	public
	@ResponseBody
	Iterable<Widget> findAll()
	{
		return widgetRepository.findAll();
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "/{widgetId}", method = RequestMethod.GET)
	public
	@ResponseBody
	Widget findOne(@PathVariable Long widgetId)
	{
		return widgetRepository.findOne(widgetId);
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "/new", method = RequestMethod.GET)
	public String newWidget()
	{
		return "wizardWidget";
	}
}
