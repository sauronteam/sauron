package com.equifax.hackaton.sauron.controller;

import com.equifax.hackaton.sauron.domain.FrequencyRefresh;
import com.equifax.hackaton.sauron.repository.FrequencyRefreshRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@RestController
@RequestMapping (FrequencyRefreshController.ENDPOINT)
public class FrequencyRefreshController
{

	@Autowired
	private FrequencyRefreshRepository frequencyRefreshRepository;

	public static final String ENDPOINT = "/frequenciesrefresh";

	@RequestMapping (method = RequestMethod.GET)
	public Iterable<FrequencyRefresh> getFreciencyFreshList()
	{
		return frequencyRefreshRepository.findAll();
	}

}
