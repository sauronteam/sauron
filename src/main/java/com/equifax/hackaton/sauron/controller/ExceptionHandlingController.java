package com.equifax.hackaton.sauron.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@ControllerAdvice
public class ExceptionHandlingController
{

	private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

	// Specify the name of a specific view that will be used to display the error:
	@ExceptionHandler (Exception.class)
	public ModelAndView handleException(HttpServletRequest req, Exception exception)
	{
		LOGGER.error("Request: " + req.getRequestURL() + " raised " + exception);
		ModelAndView mav = new ModelAndView();
		mav.addObject("exception", exception);
		mav.addObject("url", req.getRequestURL());
		mav.setViewName("error");
		return mav;
	}

}
