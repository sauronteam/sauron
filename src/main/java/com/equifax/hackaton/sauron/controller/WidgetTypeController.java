package com.equifax.hackaton.sauron.controller;

import com.equifax.hackaton.sauron.domain.WidgetType;
import com.equifax.hackaton.sauron.repository.WidgetTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@RestController
@RequestMapping (WidgetTypeController.ENDPOINT)
public class WidgetTypeController
{

	@Autowired
	private WidgetTypeRepository widgetTypeRepository;

	public static final String ENDPOINT = "/widgetstype";

	@RequestMapping (method = RequestMethod.GET)
	public Iterable<WidgetType> getWidgetTypeList()
	{
		return widgetTypeRepository.findAll();
	}
}
