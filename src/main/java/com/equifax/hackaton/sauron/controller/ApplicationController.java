package com.equifax.hackaton.sauron.controller;

import com.equifax.hackaton.sauron.domain.Application;
import com.equifax.hackaton.sauron.domain.UserDetail;
import com.equifax.hackaton.sauron.repository.ApplicationRepository;
import com.equifax.hackaton.sauron.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@Controller
@Scope ("session")
@RequestMapping (ApplicationController.ENDPOINT)
public class ApplicationController
{

	public static final String ENDPOINT = "/applications";

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ApplicationRepository applicationRepository;

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "", method = RequestMethod.GET)
	public
	@ResponseBody
	Iterable<Application> findAll()
	{
		return applicationRepository.findAll();
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "", method = RequestMethod.POST)
	public
	@ResponseBody
	Application saveApplicacion(@RequestParam ("applicationName") String name, @AuthenticationPrincipal UserDetail user)
	{
		Application app = new Application();
		app.setName(name);
		app.setUser(userRepository.findOne(user.getUserId()));
		applicationRepository.save(app);
		return app;
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "/{idApplication}", method = RequestMethod.GET)
	public
	@ResponseBody
	Application viewApplicacion(@AuthenticationPrincipal User user, Model model, @PathVariable Long idApplication)
	{
		return applicationRepository.findOne(idApplication);
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "/new", method = RequestMethod.GET)
	public String newApplicacion(@AuthenticationPrincipal User user, Model model)
	{
		model.addAttribute("applicationId", new Application());
		return "applicationForm";
	}

}
