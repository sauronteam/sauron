package com.equifax.hackaton.sauron.controller;

import com.equifax.hackaton.sauron.domain.View;
import com.equifax.hackaton.sauron.repository.ViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * @author Diego Sepúlveda Briones
 * @version %I%, %G%
 * @since 1.0.0
 */
@RestController
@RequestMapping (ViewController.ENDPOINT)
public class ViewController
{

	public static final String ENDPOINT = "/views";

	@Autowired
	private ViewRepository viewRepository;

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "", method = RequestMethod.GET)
	public
	@ResponseBody
	Iterable<View> findAll()
	{
		return viewRepository.findAll();
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "/{viewId}", method = RequestMethod.GET)
	public
	@ResponseBody
	View findOne(@PathVariable Long viewId)
	{
		return viewRepository.findOne(viewId);
	}

	@PreAuthorize ("hasAnyRole('ADMIN')")
	@RequestMapping (value = "", method = RequestMethod.POST)
	public
	@ResponseBody
	View save(@RequestParam ("viewName") String name)

	{
		View view = new View();
		view.setHtmlId(name.replace(" ", ""));
		view.setName(name);
		viewRepository.save(view);
		return view;
	}
}
