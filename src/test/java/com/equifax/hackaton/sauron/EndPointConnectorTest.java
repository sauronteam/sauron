package com.equifax.hackaton.sauron;

import com.equifax.hackaton.sauron.domain.Application;
import com.equifax.hackaton.sauron.domain.Endpoint;
import com.equifax.hackaton.sauron.domain.MetricHistory;
import com.equifax.hackaton.sauron.domain.User;
import com.equifax.hackaton.sauron.repository.ApplicationRepository;
import com.equifax.hackaton.sauron.repository.EndpointRepository;
import com.equifax.hackaton.sauron.repository.MetricHistoryRepository;
import com.equifax.hackaton.sauron.repository.UserRepository;
import com.equifax.hackaton.sauron.service.impl.EndpointServiceImpl;
import com.equifax.hackaton.sauron.service.jmx.impl.JMXMetricHandler;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import wiremock.org.mortbay.io.EndPoint;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SauronApplication.class)
@WebAppConfiguration
public class EndPointConnectorTest
{


	@Rule
	public WireMockRule wireMockRule = new WireMockRule(8089);

	@Autowired
	private EndpointServiceImpl service;

	@Autowired
	private EndpointRepository endpointRepository;

	@Autowired
	private MetricHistoryRepository metricHistoryRepository;

	@Autowired
	private ApplicationRepository applicationRepository;

	@Autowired
	private UserRepository userRepository;

	@Test
	public void jmxConnectorTest()
	{
		User newUser = createUser();
		userRepository.save(newUser);

		Application newApp = createApplication(newUser);
		applicationRepository.save(newApp);

		Endpoint undiscoverEndpoint = createEndpoint( "service:jmx:rmi:///jndi/rmi://acsd1lc9a003.app.c9.equifax.com:3614/jmxrmi", newApp);

		Endpoint discoveredEndpoint = service.discoverEndpoint(undiscoverEndpoint);

		service.captureMetricsSnaphshot(discoveredEndpoint);

		Iterable<MetricHistory> list = metricHistoryRepository.findAll();

		Assert.assertNotNull(list);
		Assert.assertTrue(list.iterator().hasNext());
		//Assert.assertTrue(discoveredEndpoint.getMetricList().size() == 8);


	}

	private Endpoint createEndpoint(String mockEndPointUrl, Application newApp)
	{
		Endpoint undiscoverEndpoint = new Endpoint();
		undiscoverEndpoint.setUrl(mockEndPointUrl);
		undiscoverEndpoint.setId(1L);
		undiscoverEndpoint.setApplication(newApp);
		return undiscoverEndpoint;
	}

	private Application createApplication(User newUser)
	{
		Application newApp = new Application();
		newApp.setId(1L);
		newApp.setName("basicApp");
		newApp.setUser(newUser);
		return newApp;
	}

	private User createUser()
	{
		User newUser = new User();
		newUser.setId(1L);
		newUser.setUsername("basicUser");
		return newUser;
	}


}
