package com.equifax.hackaton.sauron;

import com.equifax.hackaton.sauron.domain.Application;
import com.equifax.hackaton.sauron.domain.Endpoint;
import com.equifax.hackaton.sauron.domain.User;
import com.equifax.hackaton.sauron.repository.ApplicationRepository;
import com.equifax.hackaton.sauron.repository.EndpointRepository;
import com.equifax.hackaton.sauron.repository.UserRepository;
import com.equifax.hackaton.sauron.service.impl.EndpointServiceImpl;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SauronApplication.class)
@WebAppConfiguration
public class SauronApplicationTests {


	@Rule
	public WireMockRule wireMockRule = new WireMockRule(8089);

	@Autowired
	private EndpointServiceImpl service;

	@Autowired
	private EndpointRepository endpointRepository;

	@Autowired
	private ApplicationRepository applicationRepository;

	@Autowired
	private UserRepository userRepository;

	@Test
	public void endpointDiscoveryTest()
	{
		User newUser = createUser();
		userRepository.save(newUser);

		Application newApp = createApplication(newUser);
		applicationRepository.save(newApp);

		Endpoint undiscoverEndpoint = createEndpoint( "http://localhost:8089/response", newApp);

		mockRestService();

		service.discoverEndpoint(undiscoverEndpoint);

		Endpoint discoveredEndpoint = endpointRepository.findOne(undiscoverEndpoint.getId());
		Assert.assertNotNull(discoveredEndpoint);
		Assert.assertFalse(discoveredEndpoint.getMetricList().isEmpty());


	}

	private Endpoint createEndpoint(String mockEndPointUrl, Application newApp)
	{
		Endpoint undiscoverEndpoint = new Endpoint();
		undiscoverEndpoint.setUrl(mockEndPointUrl);
		undiscoverEndpoint.setId(1L);
		undiscoverEndpoint.setApplication(newApp);
		return undiscoverEndpoint;
	}

	private Application createApplication(User newUser)
	{
		Application newApp = new Application();
		newApp.setId(1L);
		newApp.setName("basicApp");
		newApp.setUser(newUser);
		return newApp;
	}

	private User createUser()
	{
		User newUser = new User();
		newUser.setId(1L);
		newUser.setUsername("basicUser");
		return newUser;
	}

	private void mockRestService()
	{
		stubFor(get(urlEqualTo("/response"))
				.willReturn(aResponse()
						.withHeader("Content-Type", "application/json")
						.withBody("{\n"
								+ "    \"counter.status.200.root\": 20,\n"
								+ "    \"counter.status.200.metrics\": 3,\n"
								+ "    \"counter.status.200.star-star\": 5,\n"
								+ "    \"counter.status.401.root\": 4,\n"
								+ "    \"gauge.response.star-star\": 6,\n"
								+ "    \"gauge.response.root\": 2,\n"
								+ "    \"gauge.response.metrics\": 3,\n"
								+ "    \"classes\": 5808,\n"
								+ "    \"classes.loaded\": 5808,\n"
								+ "    \"classes.unloaded\": 0,\n"
								+ "    \"heap\": 3728384,\n"
								+ "    \"heap.committed\": 986624,\n"
								+ "    \"heap.init\": 262144,\n"
								+ "    \"heap.used\": 52765,\n"
								+ "    \"mem\": 986624,\n"
								+ "    \"mem.free\": 933858,\n"
								+ "    \"processors\": 8,\n"
								+ "    \"threads\": 15,\n"
								+ "    \"threads.daemon\": 11,\n"
								+ "    \"threads.peak\": 15,\n"
								+ "    \"uptime\": 494836,\n"
								+ "    \"instance.uptime\": 489782,\n"
								+ "    \"datasource.primary.active\": 5,\n"
								+ "    \"datasource.primary.usage\": 0.25\n"
								+ "}")));
	}

}
